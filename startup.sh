#!/bin/bash
foreman-installer --foreman-admin-password=changeme \
  && puppet agent --test \
  && tail -f /var/log/foreman/production.log
