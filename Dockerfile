FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8 

RUN apt-get update -qq && apt-get install -y -qq apt-utils locales ca-certificates wget sudo gnupg2 python-pip && \
	ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
	cd /tmp && wget https://apt.puppetlabs.com/puppet5-release-bionic.deb && \
	dpkg -i /tmp/puppet5-release-bionic.deb && \
	echo "deb http://deb.theforeman.org/ bionic 1.21" | sudo tee /etc/apt/sources.list.d/foreman.list && \
	echo "deb http://deb.theforeman.org/ plugins 1.21" | sudo tee -a /etc/apt/sources.list.d/foreman.list && \
	sudo apt-get -y install ca-certificates && \
	wget -q https://deb.theforeman.org/pubkey.gpg -O- | sudo apt-key add - && \
	sudo apt-get update && sudo apt-get -y install foreman-installer && \
	/usr/sbin/locale-gen en_US.UTF-8 && \
	dpkg-reconfigure -f noninteractive locales && \
	/usr/sbin/update-locale LANG=en_US.UTF-8 && \
	apt-get autoremove && \
	rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /tmp/* && \
	rm -rf /var/lib/apt/* && \
	mkdir /opt/java


RUN /opt/puppetlabs/puppet/bin/puppet apply -e 'host { $::hostname: ensure => absent } -> host { "${::hostname}.docker.local": ip => $::ipaddress, host_aliases => [$::hostname] }' && \
	apt-get update && \
	foreman-installer --enable-foreman --enable-foreman-plugin-discovery --enable-puppet && \
        apt-get autoremove && \
        rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin && \
        rm -rf /var/lib/apt/lists/* && \
        rm -rf /tmp/* && \
        rm -rf /var/lib/apt/*

# rerun setup with current hostname
CMD bash /startup.sh
